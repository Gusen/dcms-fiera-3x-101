-- phpMyAdmin SQL Dump
-- version 4.3.4
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Июл 03 2015 г., 03:39
-- Версия сервера: 5.6.22-log
-- Версия PHP: 5.5.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `test_local`
--

-- --------------------------------------------------------

--
-- Структура таблицы `lang_list`
--

CREATE TABLE IF NOT EXISTS `lang_list` (
  `id` int(11) unsigned NOT NULL,
  `time` int(11) unsigned NOT NULL,
  `name` text NOT NULL,
  `avtor` varchar(99) NOT NULL DEFAULT 'default',
  `opis` text NOT NULL,
  `name_g` varchar(99) DEFAULT ''
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `lang_list`
--

INSERT DELAYED INTO `lang_list` (`id`, `time`, `name`, `avtor`, `opis`, `name_g`) VALUES
(1, 0, 'Русский', 'default', '', 'ru'),
(3, 0, 'English', 'Saint and Google', '', 'en');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `lang_list`
--
ALTER TABLE `lang_list`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `lang_list`
--
ALTER TABLE `lang_list`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
