<?php
function user_browser($id = 0)
{
	  global $set;
	  $ank = get_user($id);
	  $uslast = $ank['date_last'] > time() - $set['user_online'];
	  $browser = " <img style='margin-bottom: 1px;' src='/style/icons/".($ank['browser'] == 'web' ? 'web':'wap').".png' alt='browser'/> ";
	  if (!$uslast) $browser = null;
	  return  $browser;
}